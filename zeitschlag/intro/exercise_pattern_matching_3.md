> The variable `a` is bound to the value `2`. Which of the following will match?

- `[a, b, a] = [1, 2, 3]` ❌ as `a` was bound to `1`, so it doesn't match with `3`. The previous binding doesn't matter.
- `[a, b, a] = [1, 1, 2]` ❌ as `a` was bound to `1`, so it doesn't match with `1`. That it was bound to `2` doesn't matter.
- `a = 1` ✅ `a` is rebound, previous binding doesn't matter
- `^a = 2` ✅ We use the value of `a` (due to pin-operator) and `2=2` matches
- `^a = 1` ❌ We use the value of `a` (due to pin-operator) and `2=1` doesn't match
- `^a = 2-a` ❌ We use the value of `a` on the left _and_ right side, so it's `2 = 2-2`, which doesn't match