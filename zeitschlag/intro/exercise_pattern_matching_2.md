> Which of the following will match?

- `[a, b, a] = [1, 2, 3]` ❌ as `a` was bound to `1`, so it doesn't match with `3`
- `[a, b, a] = [1, 1, 2]` ❌ as `a` was bound to `1`, so it doesn't match with `1`
- `[a, b, a] = [1, 2, 1]` ✅