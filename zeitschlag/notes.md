# Nathan's Notes

Think of this an a notepad for generals remarks. I still take handwritten notes while reading the book.

- I already read the book two years ago but forgot almost everything.
- IEx is way more powerful than I remembered it. It feels like a pretty complete development environment with its `compile`/`recompile`/`pwd`/`ls`/... helpers
- [IEx][iex] also features autocomplete for modules
- Almost everything in Elixir seems to be command line, so it probably makes sense to get used to that. I've struggled with CLI and especially `vim` for years, but I still try to get a hang of it.
- Usually I prefer to have another editor like [BBEdit][bbedit] or [Textmate][textmate], but in that case I have to switch between windows all the time. There's an [Elixir plugin for IntelliJ][intellij-elixir], maybe this is worth a look? It might make my life easier.
- Think of match-operation as equation Elixir tries to solve. If it can make the value of left side match the value of the right side (and may it be by binding values to variables), everything is fine.
- Variables on the right-hand side of a match operation are replaced by their values.
- if `mix test` fails on an Apple Silicon-Mac, try reinstalling `brew`, `elixir` and `erlang` after disabling Rosetta ([Source][brew-m1-elixir])
- The basic set of Elixir (types, concepts) seems to be quite small (integer, floating points, atoms, ranges, regexs, collections and immutability, pattern matching). This serves a foundation, and combined those concepts/types seem to be very powerful


[intellij-elixir]: https://github.com/KronicDeth/intellij-elixir
[bbedit]: http://www.barebones.com/products/bbedit/index.html
[textmate]: https://macromates.com/
[brew-m1-elixir]: https://elixirforum.com/t/fresh-install-new-project-mix-test-causes-crash-1-14-3/54601/12
[iex]: https://hexdocs.pm/iex/IEx.html
